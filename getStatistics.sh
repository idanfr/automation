#!/bin/bash       

TXT_dir="/mnt/share/Server/R&D/ThermApp Movies/ThermApp Air/AutoRuns/OUTs_random_list_new_regression/TXTs"
GT_dir="/mnt/share/Server/R&D/ThermApp Movies/ThermApp Air/CR_T"

echo "Starting statistics.. "

# check if Files Server is mounted
if ! grep -qs '/mnt/share/Server' /proc/mounts; then
	if [ ! -d "/mnt/share/Server" ]; then
		mkdir /mnt/share/Server
	fi
	sudo mount -t cifs -o username=idanf,password=A123456!,vers=2.0 //192.168.5.10/data /mnt/share/Server
	echo "Server mounted to /mnt/share/Server"
fi

# remove old statistic file if exists
if [ -f "$TXT_dir"/statistics_outputs/all.stat ]; then
	rm "$TXT_dir"/statistics_outputs/all.stat
fi
if [ ! -d "$TXT_dir"/statistics_outputs ]; then
	mkdir "$TXT_dir"/statistics_outputs
fi



for f in "$TXT_dir"/*.txt
do
	
	echo "$f"

	FNAME_TXT=`echo "$f" | rev | cut -d'/' -f1 | rev`
	FNAME=`echo "$FNAME_TXT" | cut -d'.' -f1`
	FNUM=`echo "$FNAME" | cut -d'_' -f1`

	FNAME_GT="$GT_dir"/cr_t_"$FNUM".P.txt

	if [ -f "$FNAME_GT" ]; then
		python statistics.py "$FNAME_GT" "$f" > "$TXT_dir"/statistics_outputs/"$FNAME".stat
		python statistics.py "$FNAME_GT" "$f" >> "$TXT_dir"/statistics_outputs/all.stat
	else
		echo not exist
	fi


done


echo "Finished !"
