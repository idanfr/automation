#!/bin/bash

###################################################################
BASE_DIR="/mnt/share/Server/R&D/ThermApp Movies/ThermApp Air"
NETS_DIR="../AlgoPatternAnalysis"
###################################################################

### !!!!!!!!!!!!!!!!!!!!!!!!!!!!
### Do not change here anything!
### !!!!!!!!!!!!!!!!!!!!!!!!!!!!

print_usage() {
	echo "runAll.sh Usage: "
	echo "	./runAll.sh -o=<OUT_PATH> -n=<NET_FILE> [optional: -e=<EXIST_FILES_LIST> -d=<true/false>]"
	echo "	example:   ./runAll.sh -o=OUTs_test -n=net_dh10_3.h -e='my_raws_list.txt'"
}

#!/bin/bash

low=(-500 -200 -100 -60 -30 0 50 200)
high=(-200 -50 0 30 60 100 200 500)

file=temp_list.txt
bin_dir="../bin64"
json_file="../bin64/config.json"


for l in "${low[@]}"
do
	for h in "${high[@]}"
	do
		
		echo "High: " $h " Low: " $l

		echo "Starting automatic run..."

		IS_DISPLAY=false

		for i in "$@"
		do
		case $i in
		    -n=*|--net=*)
		    NET_FILE="${i#*=}"
		    shift # past argument=value
		    ;;
		    -o=*|--out=*)
		    OUT_PATH="${i#*=}"
		    shift # past argument=value
		    ;;
		    -e=*|--exist=*)
		    EXIST_FILE="${i#*=}"
		    shift # past argument=value
		    ;;
		    -d=*|--display=*)
		    IS_DISPLAY="${i#*=}"
		    shift # past argument=value
		    ;;
		    --default)
		    DEFAULT=YES
		    shift # past argument with no value
		    ;;
		    *)
		    # unknown option
		    ;;
		esac
		done

		# check if net file argument exists
		if [ -z "$NET_FILE" ]; then
			echo "*** Error: Net is empty!"
			print_usage
			exit
		fi

		# check if out path argument exists
		if [ -z "$OUT_PATH" ]; then
			echo "*** Error: Out path is empty!"
			print_usage
			exit
		fi

		# check if input-file exists
		if [ ! -z $EXIST_FILE ]; then
			file="$EXIST_FILE"
			echo "Using existing file: " "$EXIST_FILE"

			if [ ! -f "$file" ]; then
		    	echo "$file" " - File not found!"
				exit
			fi
		fi


		NET_FILE="$NETS_DIR"/"$NET_FILE"
		OUT_PATH="$BASE_DIR"/"$OUT_PATH"/

		# check if net file exists
		if [ ! -f "$NET_FILE" ]; then
			echo "Net file:" "$NET_FILE" " - File not found!"
			exit
		fi

		# check if out path exists
		if [ ! -d "$OUT_PATH" ]; then
			echo "Out path:" "$OUT_PATH" " - Path not found!"
			exit
		fi


		if [ -z $EXIST_FILE ]; then
			find "$BASE_DIR" -maxdepth 2 -type f -name "*.raw" > $file
		fi

		cp $json_file old_config.json


		while read -r line
		do

			echo "Processing file: " "$line"

			FNAME_RAW=`echo "$line" | rev | cut -d'/' -f1 | rev`
			FNAME=`echo "$FNAME_RAW" | cut -d'.' -f1`
			FNAME_OUT="$OUT_PATH"_$l_$h/"$FNAME".out

			if [ ! -f "$FNAME_OUT" ]; then
			    cp $json_file "$bin_dir"/temp.json
				./jq -r --arg value "$line" '.simulationApp.filename1 |= $value' $json_file > "$bin_dir"/temp.json
				cp "$bin_dir"/temp.json $json_file
				rm "$bin_dir"/temp.json

				cp $json_file "$bin_dir"/temp.json
				./jq -r --arg value "$NET_FILE" '.patternAnalysis.mNetVer |= $value' $json_file > "$bin_dir"/temp.json
				cp "$bin_dir"/temp.json $json_file
				rm "$bin_dir"/temp.json

				cp $json_file "$bin_dir"/temp.json
		 		./jq -r --arg value "$OUT_PATH" '.fileQAFilter.mOutPath |= $value' $json_file > "$bin_dir"/temp.json
				cp "$bin_dir"/temp.json $json_file
				rm "$bin_dir"/temp.json

		 		cp $json_file "$bin_dir"/temp.json
				./jq -r --arg value true '.simulationApp.mIsFileQAFilter |= $value' $json_file |
				./jq -r --arg value "$IS_DISPLAY" '.simulationApp.mIsDisplayFilter |= $value' |
				./jq -r --arg value true '.simulationApp.IsAnalytics |= $value' |
				./jq -r --arg value $l '.blobAnalysis.mThrLowAdj |= $value' |
				./jq -r --arg value $h '.blobAnalysis.mThrHighAdj |= $value' |


				./jq -r --arg value 9999 '.simulationApp.mFrameRate |= $value' > "$bin_dir"/temp.json
				cp "$bin_dir"/temp.json $json_file
				rm "$bin_dir"/temp.json

				cd ../bin64
				./Chimera
				cd ../Automation
				
			else
				echo "Out file already exist."
			fi

		done < $file


		#rm $file

		cp $json_file "$OUT_PATH"/config.json #copy the last config to OUTs path

		cp old_config.json $json_file
		rm old_config.json

		echo "Finished"

	done



done
