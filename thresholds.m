fid = fopen('Z:\R&D\ThermApp Movies\ThermApp Air\AutoRuns\Thresholds\res\threshold_analysis.txt','r');

A = textscan(fid,'%s %f %s %f %s %f');

figure; 
subplot(2,1,1); plot(A{2},A{4},'o','Color','r'); title('Low Adj');
subplot(2,1,2); plot(A{2},A{6},'o','Color','g'); title('High Adj');

fclose(fid);
