#!/bin/bash


if [ "$#" -ne 2 ]; then
	echo "Not enough parameters!"
	echo "Usage: outsConverter <OUTs_dir> <TXTs_dir>"
	exit
fi

OUTs_dir="$1"
TXTs_dir="$2"


for file in "$OUTs_dir"/*.out
do
	FNAME=`basename "$file" .out`
	TXT_NAME="$TXTs_dir"/"$FNAME".txt
	echo "$TXT_NAME"
    
    python convertOut.py "$file" "$TXT_NAME"
done