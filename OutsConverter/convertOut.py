import sys
import numpy as np


def main():

	OUT_file = sys.argv[1]
	TXT_file = sys.argv[2]

	fid_in = open(OUT_file, "r")
	fid_out = open(TXT_file, "w")

	currentFrame = 0
	line = fid_in.readline()

	while line:
		if currentFrame == 0:
			fid_out.write('Header line\n')

		if line.startswith('Frame'): # frame line
			currentFrame = line.split()[1]

		elif line.startswith('['): #vector line
			None
			
		elif line.startswith('#'): #comment line / header line
			None

		else: # result line
			spl = line.split()
			x = spl[1]
			y = spl[3]
			w = spl[5]
			h = spl[7]
			res = spl[13]

			if res == 'YES':
				res = 1
			elif res == 'NO':
				res = 0

			out_line = currentFrame + " " + x + " " + y + " " + w + " " + h + " " + str(res)
			fid_out.write(out_line + '\n')



		line = fid_in.readline()

	fid_in.close()
	fid_out.close()


if __name__ == "__main__":
	main()
