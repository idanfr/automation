import sys
import numpy as np

def bboxOverlapRatio(rect_a, rect_b, Type = 'union'): 

    Xa1 = rect_a[0];    Xa2 = rect_a[0]+rect_a[2]
    Ya1 = rect_a[1];    Ya2 = rect_a[1]+rect_a[3]
    
    Xb1 = rect_b[0];    Xb2 = rect_b[0]+rect_b[2]
    Yb1 = rect_b[1];    Yb2 = rect_b[1]+rect_b[3]


    Sa = rect_a[2] * rect_a[3]
    Sb = rect_b[2] * rect_b[3]
    Si = max(0,min(Xa2, Xb2) - max(Xa1, Xb1)) * max(0, min(Ya2, Yb2) - max(Ya1, Yb1))
    Su = Sa + Sb - Si

    
    if (Type == 'union'):
        overlap = float(Si) / float(Su)
    elif Type == 'min':
        overlap = float(Si) / float(min(Sa,Sb)) 

    return overlap



def main():
	# results_file = "analyticsTest//3196_r0_6.txt"
	# grountruth_file = "analyticsTest/GroundTruth/cr_t_3196.P.txt"
	# output_file = "analyticsTest/out.txt"

	# f = open(results_file,'r')
	# g = open(GT_file,'r') 
	# s = open(RES_file,'w')

	GT_file = sys.argv[1]
	RES_file = sys.argv[2]

	GRT = np.loadtxt(GT_file)
	RES = np.loadtxt(RES_file, skiprows=1) #skip cause of the 'Im SimulatopnApp' message
	ID_gt= GRT[:,0]; 	X_gt = GRT[:,1]; 	Y_gt = GRT[:,2];	W_gt = GRT[:,3]; 	H_gt = GRT[:,4]
	ID_rs= RES[:,0]; 	X_rs = RES[:,1]; 	Y_rs = RES[:,2];	W_rs = RES[:,3]; 	H_rs = RES[:,4];	L_rs = RES[:,5]
	

	overlap_TH = 0.25
	TP = 0
	FP = 0
	FN = 0
	# Iterate over all lines in the groundtruth file
	for frame_it in range(len(ID_gt)):
		# for each line in gt, find all lines in the result files that has
		# the same frame id as the current line.
		# then compare all bboxes to the groundtruth bbox and check it it
		# overlaps above the threshold. if yes, determine if it's a TP or
		# a FN by the detection result. if not, then it's a FP.

		isFound = False
		# frame_it = int(frame_it)
		frameID = GRT[frame_it,0]
		bbox_GRT = GRT[frame_it,1:5]
		bbox_GRT[2] = bbox_GRT[2]-bbox_GRT[0]
		bbox_GRT[3] = bbox_GRT[3]-bbox_GRT[1]

		indices = np.where(ID_rs == frameID)[0]
		for index in indices:
			# skip falses
			bbox_RES_label = RES[index,5]
			if bbox_RES_label == 0:
				continue;

			bbox_RES = RES[index,1:5]
			overlap = bboxOverlapRatio(bbox_GRT,bbox_RES,'min')
			if overlap > overlap_TH:
				isFound = True

			

		if isFound:
			TP += 1
		else:
			FN += 1

	ALL_POS 	= len(np.where(L_rs > 0)[0])
	ALL_BLOBS 	= len(ID_rs)
	ALL_GT 		= len(ID_gt)

	FP = ALL_POS - TP

	TP_ratio = float(TP)/ALL_GT
	FP_ratio = float(FP)/ALL_POS
	FN_ratio = float(FN)/ALL_GT

	out_str = (RES_file.split(".")[0]).split("/")[1] + ".raw : \t"
	out_str += 'TP: ' + str(TP) + '/' + str(ALL_GT) 	+ '(' + format(TP_ratio, '.3f') + ') \t'
	out_str += 'FP: ' + str(FP) + '/' + str(ALL_POS) 	+ '(' + format(FP_ratio, '.3f') + ') \t'
	out_str += 'FN: ' + str(FN) + '/' + str(ALL_GT) 	+ '(' + format(FN_ratio, '.3f') + ')'
	print out_str
	# s.write('TP:\t' + str(TP) + '\t(' + str(TP_ratio) + ')\n')
	# s.write('FP:\t' + str(FP) + '\t(' + str(FP_ratio) + ')\n')
	# s.write('FN:\t' + str(FN) + '\t(' + str(FN_ratio) + ')\n')

	# f.close()
	# g.close()
	# s.close()


if __name__ == "__main__":
	main()
