#!/bin/bash       

json_file="../../bin64/config.json"
test_file="config_test.json"

edit_config() {
	echo "edit_config"
	cp $json_file temp.json
	cp $test_file $json_file
	#simulationApp
	# ./jq -r --arg value false '.simulationApp.mIsFileSavingEnabled |= $value' config.json |
	# ./jq -r --arg value true  '.simulationApp.mIsBlobAnalysis |= $value' config.json |
	# ./jq -r --arg value true  '.simulationApp.mIsDepthAnalysis |= $value' config.json |
	# ./jq -r --arg value true  '.simulationApp.mIsPatternAnalysis |= $value' config.json |
	# ./jq -r --arg value true  '.simulationApp.mIsSimSensor |= $value' config.json |
	# ./jq -r --arg value false '.simulationApp.mIsDualPipeline |= $value' config.json |
	# ./jq -r --arg value false '.simulationApp.mIsDayCam |= $value' config.json |
	# ./jq -r --arg value false '.simulationApp.IsDayEncoding |= $value' config.json |
	# ./jq -r --arg value false '.simulationApp.mIsFileFilter |= $value' config.json |
	# #commFilter
	# ./jq -r --arg value "10.42.0.1" '.commFilter.mServer |= $value' config.json |
	#blobAnalysis
		# <TODO>
	#patternAnalysis
		# <TODO>
	#cp temp.json config.json
	#rm temp.json
}
display_off() {
	cp $json_file temp.json
	./jq -r --arg FILENAME false '.simulationApp.mIsDisplayFilter |= $FILENAME' $json_file > temp.json
	cp temp.json $json_file
	rm temp.json
}
replace_filename_in_config() {
	cp $json_file temp.json
	./jq -r --arg FILENAME "$1" '.simulationApp.filename1 |= $FILENAME' $json_file > temp.json
	cp temp.json $json_file
	rm temp.json
}
  
run_chimera(){
    cd ../../bin64
    FNAME_RAW=`echo $1 | rev | cut -d'/' -f1 | rev`
    echo "Now playing: " $FNAME_RAW
    FNAME=`echo $FNAME_RAW | cut -d'.' -f1`
    ./Chimera > ../Automation/analyticsTest/movies_outputs/"$FNAME".txt
    cd ../Automation/analyticsTest
}

file_parser() {
	#TODO: parse dir
	FNAME_RAW=`echo $1 | rev | cut -d'/' -f1 | rev`
	PARENT=`echo $1 | rev | cut -d'/' -f2 | rev`
	fFOCAL=`echo $1 | cut -d'_' -f5`
	aHEIGHT=`echo $1 | cut -d'_' -f8`
	dPITCH=`echo $1 | cut -d'_' -f9`
	FOCAL=${fFOCAL:1:3} 
	HEIGHT=${aHEIGHT:1:3} 
	PITCH=${dPITCH:1:3} 
	echo $FOCAL $PITCH $HEIGHT
}

test_single_movie() {
	PARSING=`file_parser "$1"`
	replace_filename_in_config "$1" "$PARSING"
	wait
	run_chimera "$1"
	wait
	FNAME_RAW=`echo "$1" | rev | cut -d'/' -f1 | rev`
	FNAME=`echo "$FNAME_RAW" | cut -d'.' -f1`
	FNUM=`echo "$FNAME" | cut -d'_' -f1`
	python statistics.py "GroundTruth/cr_t_""$FNUM"".P.txt" movies_outputs/"$FNAME".txt > statistics_outputs/"$FNAME".stat
	python statistics.py "GroundTruth/cr_t_""$FNUM"".P.txt" movies_outputs/"$FNAME".txt >> statistics_outputs/all.stat
}


############################
# main :
############################
#edit_config


echo "Starting Analytics Test.. "
cp $json_file old_config.json
cp $test_file $json_file

if ! grep -qs '/mnt/share/Server' /proc/mounts; then
	if [ ! -d "/mnt/share/Server" ]; then
		mkdir /mnt/shar/Server
	fi
	sudo mount -t cifs -o username=idanf,password=A123456!,vers=2.0 //192.168.5.10/data /mnt/share/Server
	echo "Server mounted to /mnt/share/Server"
fi

if [ ! -z "$1" ]; then
	if [ "$1" -eq 0 ]; then
		display_off
		echo "Video display is OFF"
	fi
fi

if [ -f statistics_outputs/all.stat ]; then
	rm statistics_outputs/all.stat
fi
server_path_prefix="/mnt/share/Server/R&D/ThermApp Movies/ThermApp Air/"

while read p; do
	test_single_movie "$server_path_prefix""$p"
done < raws_list.txt


echo "Finished test successfully!"

wait
cp old_config.json $json_file
rm old_config.json
# wait
# cp old_config.json config.json
# rm old_config.json


