#!/bin/bash       

TXT_dir="/mnt/share/Server/R&D/ThermApp Movies/ThermApp Air/AutoRuns/Thresholds/res"
GT_dir="/mnt/share/Server/R&D/ThermApp Movies/ThermApp Air/CR_T"

echo "Starting threshold analysis.. "



if [ -f "$TXT_dir"/threshold_analysis.txt ]; then
	rm "$TXT_dir"/threshold_analysis.txt
fi

for f in "$TXT_dir"/*.txt
do
	
	echo "$f"

	FNAME_TXT=`echo "$f" | rev | cut -d'/' -f1 | rev`
	FNAME=`echo "$FNAME_TXT" | cut -d'.' -f1`
	FNUM=`echo "$FNAME" | cut -d'_' -f1`
	FNAME_GT="$GT_dir"/cr_t_"$FNUM".P.txt

	if [ -f "$FNAME_GT" ]; then
		python analyzeThresholds.py "$FNAME_GT" "$f" >> "$TXT_dir"/threshold_analysis.txt
	else
		echo not exist
	fi


done


echo "Finished !"
