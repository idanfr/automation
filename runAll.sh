#!/bin/bash

###################################################################
NETS_DIR="../AlgoPatternAnalysis"
###################################################################

### !!!!!!!!!!!!!!!!!!!!!!!!!!!!
### Do not change here anything!
### !!!!!!!!!!!!!!!!!!!!!!!!!!!!

print_usage() {
	echo "runAll.sh Usage: "
	echo "	./runAll.sh -o=<OUT_PATH> -n=<NET_FILE> -p=<PROJECT> [optional: -e=<EXIST_FILES_LIST> -d=<true/false>]"
	echo "	example:   ./runAll.sh -o=OUTs_test -n=net_dh10_3.h -p=Chimera -e='my_raws_list.txt'"
}


file=temp_list.txt
bin_dir="../bin64"
json_file="../bin64/config.json"


echo "Starting automatic run..."

IS_DISPLAY=false
IS_VECTOR=true

for i in "$@"
do
case $i in
    -n=*|--net=*)
    NET_FILE="${i#*=}"
    shift # past argument=value
    ;;
    -n1=*|--net1=*)
    NET_FILE1="${i#*=}"
    shift # past argument=value
    ;;
    -o=*|--out=*)
    OUT_PATH="${i#*=}"
    shift # past argument=value
    ;;
    -e=*|--exist=*)
    EXIST_FILE="${i#*=}"
    shift # past argument=value
    ;;
    -d=*|--display=*)
    IS_DISPLAY="${i#*=}"
    shift # past argument=value
    ;;
    -v=*|--vector=*)
    IS_VECTOR="${i#*=}"
    shift # past argument=value
    ;;
    -p=*|--project=*) 
    PROJECT="${i#*=}"
    shift # past argument=value
    ;;
    --default)
    DEFAULT=YES
    shift # past argument with no value
    ;;
    *)
    # unknown option
    ;;
esac
done

# check if net file argument exists
if [ -z "$NET_FILE" ]; then
	echo "*** Error: Net is empty!"
	print_usage
	exit
fi

# check if out path argument exists
if [ -z "$OUT_PATH" ]; then
	echo "*** Error: Out path is empty!"
	print_usage
	exit
fi

# check if input-file exists
if [ ! -z $EXIST_FILE ]; then
	file="$EXIST_FILE"
	echo "Using existing file: " "$EXIST_FILE"

	if [ ! -f "$file" ]; then
    	echo "$file" " - File not found!"
		exit
	fi
fi


# according to project -> choose a different OUTS_DIR
case "$PROJECT" in
	Medusa)		
		OUTS_DIR="/mnt/share/Server/Local/OUTs/OpenFrame DroneDetect"
	
	;;

	Chimera)
		OUTS_DIR="/mnt/share/Server/Local/OUTs/ThermApp Air"
	;;

	*)
	#unknown option
	;;
esac

NET_FILE="$NETS_DIR"/"$NET_FILE"
NET_FILE1="$NETS_DIR"/"$NET_FILE1"
OUT_PATH="$OUTS_DIR"/"$OUT_PATH"/

# check if net file exists
if [ ! -f "$NET_FILE" ]; then
	echo "Net file:" "$NET_FILE" " - File not found!"
	exit
fi
if [ ! -f "$NET_FILE1" ]; then
	echo "Net file 2:" "$NET_FILE1" " - File not found! using net0 as default."
fi

# check if out path exists
if [ ! -d "$OUT_PATH" ]; then
	echo "Out path:" "$OUT_PATH" " - Path not found!"
	exit
fi


if [ -z $EXIST_FILE ]; then

# according to project -> choose a different BASE_DIR and diffrent file suffix
	case "$PROJECT" in
		Medusa)
			BASE_DIR="/mnt/share/Server/ThirdEye Dropbox/Production/Database/Movies/OpenFrame Movies/OpenFrame DroneDetect"

			find "$BASE_DIR" -maxdepth 2 -type f -name "*.raw2" > $file	
		;;

		Chimera)
			BASE_DIR="/mnt/share/Server/ThirdEye Dropbox/Production/Database/Movies/ThermApp Movies/ThermApp Air"

			find "$BASE_DIR" -maxdepth 2 -type f -name "*.raw" > $file	
		;;

		*)
		#unknown option
		;;
	esac

fi

cp $json_file old_config.json


while read -r line
do

	echo "Processing file: " "$line"

# according to the project save the correct out name with its corresponding suffix ( out/out2 )
		case "$PROJECT" in
			Medusa)
				FNAME_RAW=`echo "$line" | rev | cut -d'/' -f1 | rev`
				FNAME=`echo "$FNAME_RAW" | cut -d'.' -f1`
				FNAME_OUT="$OUT_PATH"/"$FNAME".out2
			;;

			Chimera)
				FNAME_RAW=`echo "$line" | rev | cut -d'/' -f1 | rev`
				FNAME=`echo "$FNAME_RAW" | cut -d'.' -f1`
				FNAME_OUT="$OUT_PATH"/"$FNAME".out
			;;

			*)
			#unknown option
			;;
		esac


echo "$FNAME_OUT"
	if [ ! -f "$FNAME_OUT" ]; then
		if [ $PROJECT == "Chimera" ]; then
	 		cp $json_file "$bin_dir"/temp.json
			./jq -r --arg value true '.simulationApp.mIsFileQAFilter |= $value' $json_file |
			./jq -r --arg value "$line" '.simulationApp.filename1 |= $value' |
			./jq -r --arg value "$NET_FILE" '.patternAnalysis.mNetVer |= $value' |
			./jq -r --arg value "$NET_FILE1" '.patternAnalysis.mNetVer2 |= $value' |
			./jq -r --arg value "$OUT_PATH" '.fileQAFilter.mOutPath |= $value' |
			./jq -r --arg value "$IS_DISPLAY" '.simulationApp.mIsDisplayFilter |= $value' |
			./jq -r --arg value "$IS_VECTOR" '.fileQAFilter.mIsPrintVector |= $value' |
			./jq -r --arg value true '.simulationApp.IsAnalytics |= $value' |
			./jq -r --arg value 9999 '.simulationApp.mFrameRate |= $value' > "$bin_dir"/temp.json
			cp "$bin_dir"/temp.json $json_file
			rm "$bin_dir"/temp.json
		elif [ $PROJECT == "Medusa" ]; then
	 		cp $json_file "$bin_dir"/temp.json
			./jq -r --arg value true '.medusaMaster.mIsFileQAFilter |= $value' $json_file |
			./jq -r --arg value "$line" '.medusaMaster.filename1 |= $value' |
			./jq -r --arg value "$NET_FILE" '.patternAnalysis.mNetVer |= $value' |
			./jq -r --arg value "$NET_FILE1" '.patternAnalysis.mNetVer2 |= $value' |
			./jq -r --arg value "$OUT_PATH" '.fileQAFilter.mOutPath |= $value' |
			./jq -r --arg value true '.medusaMaster.mIsFileQAFilter |= $value' $json_file |
			./jq -r --arg value "$IS_DISPLAY" '.medusaMaster.mIsDisplayFilter |= $value' |
			./jq -r --arg value "$IS_VECTOR" '.fileQAFilter.mIsPrintVector |= $value' |
			./jq -r --arg value true '.medusaMaster.IsAnalytics |= $value' |
			./jq -r --arg value 9999 '.medusaMaster.mFrameRate |= $value' > "$bin_dir"/temp.json
			cp "$bin_dir"/temp.json $json_file
			rm "$bin_dir"/temp.json
		fi 

		cd ../bin64
		./Chimera
		cd ../Automation
		
	else
		echo "Out file already exist."
	fi

done < $file


#rm $file

cp $json_file "$OUT_PATH"/config.json #copy the last config to OUTs path

cp old_config.json $json_file
rm old_config.json

echo "Finished"
